import threading
import time

import utils

vk_thread = threading.Thread()
tg_thread = threading.Thread()
tg_sync_thread = threading.Thread()
health_check_thread = threading.Thread()

vk_func = None
tg_func = None
tg_sync_func = None
health_check_func = None


def check_thread_alive(thr: threading.Thread):
    if thr.native_id == threading.get_native_id():
        return True
    thr.join(timeout=0.0)
    return thr.is_alive()


def health_check():
    global vk_thread
    global tg_thread
    global tg_sync_thread
    global health_check_thread

    global vk_func
    global tg_func
    global tg_sync_func
    global health_check_func

    health_is_ok = True

    msg = '===== Состояние бота =====\n'
    msg += 'VK: ['
    if check_thread_alive(vk_thread):
        msg += 'OK]\n'
    else:
        health_is_ok = False
        msg += 'FAIL]\n'
        vk_thread = threading.Thread(target=vk_func)
        vk_thread.start()
    
    msg += 'TG: ['
    if check_thread_alive(tg_thread):
        msg += 'OK]\n'
    else:
        health_is_ok = False
        msg += 'FAIL]\n'
        tg_thread = threading.Thread(target=tg_func)
        tg_thread.start()

    msg += 'TG Sync: ['
    if check_thread_alive(tg_sync_thread):
        msg += 'OK]\n'
    else:
        health_is_ok = False
        msg += 'FAIL]\n'
        tg_sync_thread = threading.Thread(target=tg_sync_func)
        tg_sync_thread.start()

    msg += 'Health check: ['
    if check_thread_alive(health_check_thread):
        msg += 'OK]\n'
    else:
        health_is_ok = False
        msg += 'FAIL]\n'
        health_check_thread = threading.Thread(target=health_check_func)
        health_check_thread.start()

    if health_is_ok:
        msg += 'Похоже, что всё ок!\n'
    else:
        msg += 'Упавшие потоки перезапущены!\n'
    msg += '===== Состояние бота ====='

    return msg


def init_bot(vk, tg, tg_sync):
    global vk_thread
    global tg_thread
    global tg_sync_thread
    global health_check_thread

    global vk_func
    global tg_func
    global tg_sync_func
    global health_check_func

    vk_func = vk
    tg_func = tg
    tg_sync_func = tg_sync
    health_check_func = health_thread_func

    vk_thread = threading.Thread(target=vk_func)
    tg_thread = threading.Thread(target=tg_func)
    tg_sync_thread = threading.Thread(target=tg_sync_func)
    health_check_thread = threading.Thread(target=health_check_func)


def health_thread_func():
    while True:
        try:
            health_check()
            time.sleep(60)
        except Exception as e:
            utils.save_exception('health_thread_func', e)
            continue
        except:
            continue


def start_bot():
    global vk_thread
    global tg_thread
    global tg_sync_thread
    global health_check_thread

    vk_thread.start()
    tg_thread.start()
    tg_sync_thread.start()
    health_check_thread.start()

    while True:
        vk_thread.join()
        tg_thread.join()
        tg_sync_thread.join()
        health_check_thread.join()
