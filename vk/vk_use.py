import json
import requests
import os

from vk_api.utils import get_random_id

import utils.file_utils as utils
from utils.poll_data import PollData

import vk.vk_utils as vk_utils

from vk.vk_vars import vk_lock
from vk.vk_vars import vk


def vk_get_user_tag(user_id):
    return f'@id{user_id}'


def vk_send_message(msg: str, chat_id_filename: str, reply_to=None):
    chat_id = int(utils.get_from_file(chat_id_filename))
    with vk_lock:
        if reply_to:
            reply_to = int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id)))
            fwd = vk_utils.gen_reply_json(chat_id, reply_to)
            res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message=msg, random_id=get_random_id(),
                                   forward=json.dumps(fwd))
        else:
            res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message=msg, random_id=get_random_id())
        utils.save_to_file('./msg_ids/vk/latest_' + str(chat_id),
                           int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id))) + 1)
        return 1


def vk_send_content(message, content, content_types, chat_id_filename: str, reply_to=None):
    chat_id = int(utils.get_from_file(chat_id_filename))

    upload_photo_server = vk.photos.getMessagesUploadServer()
    upload_photo_url = upload_photo_server["upload_url"]

    upload_doc_server = vk.docs.getMessagesUploadServer(type='doc', peer_id=2000000000 + chat_id)
    upload_doc_url = upload_doc_server["upload_url"]

    attachments = ''
    for i in range(len(content)):
        if content_types[i] == 'photo':
            res = requests.post(upload_photo_url, files={'photo': open(content[i], 'rb')}).json()
            photos = vk.photos.saveMessagesPhoto(photo=res["photo"], server=res["server"], hash=res["hash"])
            attachments += f',photo{photos[0]["owner_id"]}_{photos[0]["id"]}'
        elif content_types[i] == 'doc':
            res = requests.post(upload_doc_url, files={'file': open(content[i], 'rb')}).json()
            file = vk.docs.save(file=res["file"], title=os.path.basename(content[i]))
            attachments += f',doc{file["doc"]["owner_id"]}_{file["doc"]["id"]}'

    with vk_lock:
        if reply_to:
            reply_to = int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id)))
            fwd = vk_utils.gen_reply_json(chat_id, reply_to)
            if message:
                res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message=message,
                                       attachment=attachments[1:], random_id=get_random_id(),
                                       forward=json.dumps(fwd))
            else:
                res = vk.messages.send(peer_ids=str(2000000000 + chat_id), attachment=attachments[1:],
                                       random_id=get_random_id(), forward=json.dumps(fwd))
        else:
            if message:
                res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message=message,
                                       attachment=attachments[1:], random_id=get_random_id())
            else:
                res = vk.messages.send(peer_ids=str(2000000000 + chat_id), attachment=attachments[1:],
                                       random_id=get_random_id())
    utils.save_to_file('./msg_ids/vk/latest_' + str(chat_id),
                       int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id))) + 1)
    return 1


def vk_send_poll(poll: PollData, chat_id_filename: str, reply_to=None):
    chat_id = int(utils.get_from_file(chat_id_filename))
    with vk_lock:
        if reply_to:
            reply_to = int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id)))
            fwd = vk_utils.gen_reply_json(chat_id, reply_to)
            res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message="[боты ВК не могут отправлять опросы :(]", random_id=get_random_id(),
                                   forward=json.dumps(fwd))
        else:
            res = vk.messages.send(peer_ids=str(2000000000 + chat_id), message="[боты ВК не могут отправлять опросы :(]", random_id=get_random_id())
        utils.save_to_file('./msg_ids/vk/latest_' + str(chat_id),
                           int(utils.get_from_file('./msg_ids/vk/latest_' + str(chat_id))) + 1)
        return 1


def vk_send(chat_id_filename: str, message=None, content=None, content_types=None, poll: PollData = None, reply_to = None):
    ret = None
    if message and content:
        ret = vk_send_content(message, content, content_types, chat_id_filename, reply_to)
    elif message:
        ret = vk_send_message(message, chat_id_filename, reply_to)
    if poll:
        ret = vk_send_poll(poll, chat_id_filename, reply_to)
    return ret
