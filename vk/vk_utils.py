from vk.vk_vars import vk


def gen_reply_json(chat_id, converstion_message_id):
    return {
        'owner_id': None,
        'peer_id': 2000000000 + chat_id,
        'conversation_message_ids': [converstion_message_id],
        'message_ids': None,
        'is_reply': True
    }


def check_user_in_conversation(user_id, chat_id):
    res = vk.messages.get_conversation_members(peer_id=str(2000000000 + int(chat_id)))
    for user in res["items"]:
        if user_id == user["member_id"]:
            return True
    return False
