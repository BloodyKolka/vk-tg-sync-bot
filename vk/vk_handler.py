import json
import random

from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id

import utils.file_utils as utils
import vk.vk_utils as vk_utils
from global_vars import homework_manager

from vk.vk_vars import vk
from vk.vk_vars import vk_lock
from vk.vk_vars import vk_session
from vk.vk_vars import vk_admins
from vk.vk_vars import vk_group_id

from vk.vk_process import process_vk_message

from bot_threads import health_check


def run_vk_bot():
    longpoll = VkBotLongPoll(vk_session, 216404083)
    while True:
        try:
            for event in longpoll.listen():
                if event.type == VkBotEventType.MESSAGE_NEW:
                    if event.message:
                        if event.message.is_cropped:
                            message = vk.messages.get_by_conversation_message_id(peer_id=f'{event.message.peer_id}', conversation_message_ids=f'{event.message.conversation_message_id}')["items"][0]
                        else:
                            message = event.message
                        utils.save_to_file('./msg_ids/vk/latest_' + str(event.chat_id), event.message.conversation_message_id)
                    else:
                        continue

                    if message["text"]:
                        if message["text"] == '/health_check':
                            with vk_lock:
                                if event.from_chat:
                                    vk.messages.send(chat_id=event.chat_id, message=health_check(),
                                                     random_id=get_random_id())
                                else:
                                    vk.messages.send(user_id=message["from_id"], message=health_check(),
                                                     random_id=get_random_id())
                            continue
                        elif '/add_auf' in message["text"]:
                            if str(event.message.from_id) not in vk_admins:
                                continue

                            new_quote = message["text"].replace('/add_auf ', '')

                            f = open('./data/wolf_quotes.json')
                            quotes_file = json.load(f)
                            f.close()

                            quotes_file["quotes"].append(new_quote)

                            utils.save_to_file('./data/wolf_quotes.json', json.dumps(quotes_file, indent=4))
                            continue
                        elif message["text"] == '/auf':
                            f = open('./data/wolf_quotes.json')
                            quotes = json.load(f)["quotes"]
                            f.close()

                            msg = f'"{random.choice(quotes)}" - АУФ.'
                            with vk_lock:
                                if event.from_chat:
                                    vk.messages.send(chat_id=event.chat_id, message=msg, random_id=get_random_id())
                                else:
                                    vk.messages.send(user_id=message["from_id"], message=msg, random_id=get_random_id())
                            continue
                        elif '/callme ' in message["text"]:
                            nick = message["text"].replace('/callme ', '')
                            if not nick:
                                continue

                            f = open('./data/vk_nicknames.json')
                            nicknames = json.load(f)
                            nicknames[str(message["from_id"])] = nick
                            f.close()
                            utils.save_to_file('./data/vk_nicknames.json', json.dumps(nicknames, indent=4))

                            with vk_lock:
                                if event.from_chat:
                                    fwd = vk_utils.gen_reply_json(event.chat_id, event.message.conversation_message_id)
                                    vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                     message=f'Хорошо, теперь в Telegram я буду подписывать выши сообщения "{nick}"',
                                                     random_id=get_random_id(), forward=json.dumps(fwd))
                                else:
                                    vk.messages.send(user_id=message["from_id"],
                                                     message=f'Хорошо, теперь в Telegram я буду подписывать выши сообщения "{nick}"',
                                                     random_id=get_random_id())
                            continue
                        elif '/set_tag ' in message["text"]:
                            new_tag = message["text"].replace('/set_tag ', '')
                            new_tag = new_tag.replace(' ', '')
                            new_tag = new_tag.lower()

                            if not new_tag:
                                continue
                            new_tag = '$' + new_tag

                            f = open('./data/vk_tags.json')
                            tags = json.load(f)
                            f.close()

                            if new_tag in tags:
                                with vk_lock:
                                    if event.from_chat:
                                        fwd = vk_utils.gen_reply_json(event.chat_id, event.message.conversation_message_id)
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message=f'Упс! Тег "{new_tag}" уже занят!',
                                                         random_id=get_random_id(), forward=json.dumps(fwd))
                                    else:
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message=f'Упс! Тег "{new_tag}" уже занят!',
                                                         random_id=get_random_id())
                                continue

                            tag_to_delete = ''
                            for tag in tags:
                                if tags[tag] == str(message["from_id"]):
                                    tag_to_delete = tag
                                    break
                            if tag_to_delete:
                                del tags[tag_to_delete]

                            tags[new_tag] = str(message["from_id"])

                            utils.save_to_file('./data/vk_tags.json', json.dumps(tags, indent=4))
                            with vk_lock:
                                if event.from_chat:
                                    fwd = vk_utils.gen_reply_json(event.chat_id, event.message.conversation_message_id)
                                    vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                     message=f'Хорошо, теперь в Telegram Вас можно тегнуть с помощью "{new_tag}"',
                                                     random_id=get_random_id(), forward=json.dumps(fwd))
                                else:
                                    vk.messages.send(user_id=message["from_id"],
                                                     message=f'Хорошо, теперь в Telegram Вас можно тегнуть с помощью "{new_tag}"',
                                                     random_id=get_random_id())
                            continue
                        elif '/register_hw' in message["text"]:
                            if event.from_chat:
                                continue
                            key = message["text"].replace('/register_hw ', '')
                            if key == 'x5WQzhnXY9csm5PRn37PfxUdeskcKS5iK57HnCFt':
                                utils.add_hw_provider(event.message.from_id, 'vk')
                                with vk_lock:
                                    vk.messages.send(user_id=message["from_id"],
                                                     message='Отлично, теперь у тебя есть права добалять ДЗ!',
                                                     random_id=get_random_id())
                            else:
                                with vk_lock:
                                    vk.messages.send(user_id=message["from_id"],
                                                     message='Ошибка! Неверный ключ!',
                                                     random_id=get_random_id())
                            continue
                        elif '/add_hw' in message["text"]:
                            if event.message.from_id not in utils.get_hw_providers('vk'):
                                with vk_lock:
                                    if event.from_chat:
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message='Ошибка, у тебя нет прав добавлять ДЗ',
                                                         random_id=get_random_id())
                                    else:
                                        vk.messages.send(user_id=message["from_id"],
                                                         message='Ошибка, у тебя нет прав добавлять ДЗ',
                                                         random_id=get_random_id())
                                continue

                            args_str = message["text"].replace('/add_hw ', '')
                            subject = args_str.split(' ')[0]
                            task = args_str.replace(subject + ' ', '')
                            with vk_lock:
                                if event.from_chat:
                                    vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                     message=homework_manager.add_homework(subject, task),
                                                     random_id=get_random_id())
                                else:
                                    vk.messages.send(user_id=message["from_id"],
                                                     message=homework_manager.add_homework(subject, task),
                                                     random_id=get_random_id())
                            continue
                        if event.from_chat:
                            if '#nosync' in message["text"]:
                                continue
                            elif message["text"] == '/del':
                                if 'reply_message' not in message:
                                    continue
                                msg_id = message["reply_message"]["conversation_message_id"]
                                try:
                                    with vk_lock:
                                        vk.messages.delete(peer_id=event.message.peer_id, cmids=f'{msg_id}', group_id=vk_group_id, delete_for_all=1)
                                except:
                                    pass
                                continue
                            elif message["text"] == '/start_important':
                                if str(event.message.from_id) not in vk_admins:
                                    with vk_lock:
                                        vk.messages.send(chat_id=event.chat_id, message='Упс! У Вас нет на это прав!',
                                                         random_id=get_random_id())
                                    continue
                                utils.save_to_file('./chat_ids/vk_imp_id', event.chat_id)
                                with vk_lock:
                                    vk.messages.send(chat_id=event.chat_id, message='Добавил к синхронизации Важное!',
                                                     random_id=get_random_id())
                                continue
                            elif message["text"] == '/start_chat':
                                if str(event.message.from_id) not in vk_admins:
                                    with vk_lock:
                                        vk.messages.send(chat_id=event.chat_id, message='Упс! У Вас нет на это прав!',
                                                         random_id=get_random_id())
                                    continue
                                utils.save_to_file('./chat_ids/vk_chat_id', event.chat_id)
                                with vk_lock:
                                    vk.messages.send(chat_id=event.chat_id,
                                                     message='Добавил к синхронизации Разговорная!',
                                                     random_id=get_random_id())
                                continue
                            elif message["text"] == '/start_rpg':
                                if str(event.message.from_id) not in vk_admins:
                                    with vk_lock:
                                        vk.messages.send(chat_id=event.chat_id, message='Упс! У Вас нет на это прав!',
                                                         random_id=get_random_id())
                                    continue
                                utils.save_to_file('./chat_ids/vk_rpg_id', event.chat_id)
                                with vk_lock:
                                    vk.messages.send(chat_id=event.chat_id, message='Добавил к синхронизации НРИ!',
                                                     random_id=get_random_id())
                                continue
                            elif 'get_hw' in message["text"]:
                                if str(event.chat_id) != utils.get_from_file('./chat_ids/vk_chat_id'):
                                    continue
                                args = message["text"].replace('/get_hw ', '')
                                args = args.replace('/get_hw', '')
                                if not args.replace(' ', ''):
                                    with vk_lock:
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message=homework_manager.get_homework(),
                                                         random_id=get_random_id())
                                elif 'all' in args:
                                    with vk_lock:
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message=homework_manager.get_all_homework(),
                                                         random_id=get_random_id())
                                else:
                                    with vk_lock:
                                        vk.messages.send(peer_ids=str(2000000000 + event.chat_id),
                                                         message=homework_manager.get_homework(args.replace(' ', '')),
                                                         random_id=get_random_id())
                                continue

                    chat_id_name = str()
                    val = utils.get_from_file('./chat_ids/vk_chat_id')
                    if val == str(event.chat_id):
                        chat_id_name = './chat_ids/tg_chat_id'
                    val = utils.get_from_file('./chat_ids/vk_imp_id')
                    if val == str(event.chat_id):
                        chat_id_name = './chat_ids/tg_imp_id'
                    val = utils.get_from_file('./chat_ids/vk_rpg_id')
                    if val == str(event.chat_id):
                        chat_id_name = './chat_ids/tg_rpg_id'
                    if not chat_id_name:
                        continue

                    process_vk_message(message, chat_id_name)
        except Exception as e:
            utils.save_exception('run_vk', e)
            continue
        finally:
            continue
