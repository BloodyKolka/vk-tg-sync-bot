import json
import os

from telebot.util import escape

import utils.file_utils as utils
from utils.poll_data import PollData

from vk.vk_vars import vk_lock
from vk.vk_vars import vk_group_id
from vk.vk_vars import vk

from tg.tg_use import tg_send
from tg.tg_use import tg_get_user_tag


def process_vk_message(message, chat_id_name: str, first=True):
    msg = str()
    current_id = 0

    reply_id = 0
    if 'reply_message' in message:
        reply_id = process_vk_message(message["reply_message"], chat_id_name)

    sender_id = message["from_id"]
    if sender_id < 0:
        if sender_id != -vk_group_id:
            with vk_lock:
                groups_info = vk.groups.getById(groups_ids=f'{sender_id}')
            msg = f'<b>{groups_info[0]["name"]}</b>'
    else:
        f = open('./data/vk_nicknames.json')
        nicknames = json.load(f)
        f.close()
        if str(message["from_id"]) in nicknames:
            msg = f'<b>{nicknames[str(message["from_id"])]}</b>:'
        else:
            with vk_lock:
                users = vk.users.get(user_ids=f'{message["from_id"]}')
            msg = f'<b>{users[0]["first_name"]} {users[0]["last_name"]}</b>:'

    has_message = first
    if message["text"]:
        escaped_text = escape(message["text"])

        f = open('./data/tg_tags.json')
        tags = json.load(f)
        f.close()
        for tag in tags:
            if tag in escaped_text:
                user_tag = tg_get_user_tag(tags[tag], chat_id_name)
                if not user_tag:
                    continue
                escaped_text = escaped_text.replace(tag, user_tag)

        msg += ('\n' + escaped_text)
        has_message = True

    if first and ('fwd_messages' in message):
        if message["fwd_messages"]:
            msg += '\n[forwarding]'

    poll_data = None
    if not message["attachments"]:
        if has_message:
            current_id = tg_send(chat_id_name, message=msg, reply_id=reply_id)
    else:
        if (len(message["attachments"]) == 1) and (message["attachments"][0]["type"] == 'wall'):
            msg += '\n[Репост (см. ВК)]'
            current_id = tg_send(chat_id_name, message=msg, reply_id=reply_id)
        else:
            content = []
            content_types = []
            for attachment in message["attachments"]:
                name = './vk_tmp/'
                if attachment["type"] == 'photo':
                    photo = attachment["photo"]
                    name += f'{photo["id"]}.jpg'
                    max_height = 0
                    max_size_url = str()
                    for size in photo["sizes"]:
                        if size["height"] > max_height:
                            max_height = size["height"]
                            max_size_url = size["url"]
                    utils.download_and_save_file(max_size_url, name)
                    content.append(name)
                    content_types.append('photo')
                elif attachment["type"] == 'sticker':
                    sticker = attachment["sticker"]
                    name += f'{sticker["sticker_id"]}.png'
                    max_height = 0
                    max_size_url = str()
                    for size in sticker["images_with_background"]:
                        if size["height"] > max_height:
                            max_height = size["height"]
                            max_size_url = size["url"]
                    utils.download_and_save_file(max_size_url, name)
                    content.append(name)
                    content_types.append('photo')
                elif attachment["type"] == 'doc':
                    doc = attachment["doc"]
                    name += f'{doc["title"]}-{doc["id"]}.{doc["ext"]}'
                    if doc["size"] > 50000000:
                        name += '.txt'
                        utils.save_to_file(name, f'Файл слищком большой, скачать: {doc["url"]}')
                    else:
                        utils.download_and_save_file(doc["url"], name)
                    content.append(name)
                    content_types.append('doc')
                elif attachment["type"] == 'wall':
                    msg += '\n[Репост (см. ВК)]\n'
                elif attachment["type"] == 'link':
                    msg += f'\n{attachment["link"]["url"]}\n'
                elif attachment["type"] == 'poll':
                    poll = attachment["poll"]
                    options = list[str]()
                    for answer in poll["answers"]:
                        options.append(answer["text"])
                    poll_data = PollData(poll["question"], "", options, poll["anonymous"], poll["multiple"])
                else:
                    continue

            current_id = tg_send(chat_id_name, message=msg, content=content, content_types=content_types, poll=poll_data, reply_id=reply_id)
            if content:
                for file in content:
                    os.remove(file)

    if 'fwd_messages' in message:
        for fwd_message in message["fwd_messages"]:
            process_vk_message(fwd_message, chat_id_name, False)

    return current_id
