import requests
import json
import os

from datetime import datetime


def add_hw_provider(user_id: int, platform: str):
    if platform not in ['tg', 'vk']:
        return list()
    with open('./data/hw_providers.json', encoding='utf-8', mode='r') as f:
        obj = json.load(f)
    if user_id not in obj[platform]:
        obj[platform].append(user_id)
        with open('./data/hw_providers.json', encoding='utf-8', mode='w') as f:
            json.dump(obj, f, indent=4, ensure_ascii=False)
        return 'Добавил!'
    return "Ты и так есть в списке"


def get_hw_providers(platform: str):
    if platform not in ['tg', 'vk']:
        return list()
    with open('./data/hw_providers.json', encoding='utf-8', mode='r') as f:
        obj = json.load(f)
    return obj[platform]


def get_from_file(filename: str):
    if os.path.exists(filename) and os.path.isfile(filename):
        f = open(filename, 'r', encoding='utf-8')
        val = f.read()
        f.close()
        return val
    else:
        return 0


def save_to_file(filename: str, val):
    f = open(filename, 'w', encoding='utf-8')
    f.write(str(val))
    f.close()


def download_and_save_file(url, filename):
    r = requests.get(url, allow_redirects=True)
    open(filename, 'wb').write(r.content)


def save_exception(subfolder, e: Exception):
    filename = './exceptions/' + subfolder + datetime.now().isoformat()
    save_to_file(filename, str(e))
