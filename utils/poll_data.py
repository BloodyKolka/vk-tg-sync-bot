from dataclasses import dataclass

@dataclass
class PollData:
    title: str
    additional_text: str
    options: list[str]

    is_private: bool
    is_multiple: bool
