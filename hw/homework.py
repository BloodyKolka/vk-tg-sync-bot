from dataclasses import dataclass


@dataclass
class HomeWork:
    subject: str
    task: str

    def pretty_print(self):
        return f'* {self.subject}:\n{self.task}'

    def to_json(self):
        obj = dict()
        obj["subject"] = self.subject
        obj["task"] = self.task
        return obj

    @classmethod
    def to_json_list(cls, lst):
        res = list()
        for item in lst:
            res.append(item.to_json())
        return res

    @classmethod
    def from_json(cls, obj):
        return cls(
            subject=obj["subject"],
            task=obj["task"]
        )

    @classmethod
    def from_json_list(cls, lst: list):
        res = list()
        for item in lst:
            res.append(cls.from_json(item))
        return res
