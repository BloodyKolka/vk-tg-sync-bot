from dataclasses import dataclass

from hw.day import Day


@dataclass
class Week:
    week_type: str
    days: list[Day]

    def add_task(self, subject: str, task: str) -> bool:
        for day in self.days:
            if day.add_task(subject, task):
                return True
        return False

    def pretty_print(self):
        res = f'-- Задание на неделю ({self.week_type}) --\n'
        for day in self.days:
            res += day.pretty_print()
            res += '\n\n'
        return res

    def to_json(self):
        obj = dict()
        obj["week_type"] = self.week_type
        obj["days"] = Day.to_json_list(self.days)
        return obj

    @classmethod
    def from_json(cls, obj):
        return cls(
            week_type=obj["week_type"],
            days=Day.from_json_list(obj["days"])
        )
