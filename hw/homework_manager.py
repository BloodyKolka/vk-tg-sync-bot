import json
import threading
from datetime import datetime
from datetime import timedelta

from hw.week import Week


class HomeworkManager:
    def __init__(self, work_file: str):
        self.lock = threading.Lock()
        self.work_file = work_file

    @staticmethod
    def __get_week_number_first_semester(week_count: int):
        first_day = datetime(datetime.now().year, 9, 1)
        if first_day.weekday() <= 5:
            return first_day.isocalendar().week - week_count + 1
        else:
            return first_day.isocalendar().week + 1 - week_count + 1

    @staticmethod
    def __get_week_number_second_semester(week_count: int):
        return 0

    @staticmethod
    def __get_week_number(date: datetime):
        if 9 <= date.month.numerator <= 12:
            return HomeworkManager.__get_week_number_first_semester(date.isocalendar().week)
        elif 1 <= date.month.numerator <= 6:
            return HomeworkManager.__get_week_number_second_semester(date.isocalendar().week)

    def get_all_homework(self):
        with self.lock:
            with open(f'{self.work_file}', encoding='utf-8', mode='r') as f:
                obj = json.load(f)
        odd_week = Week.from_json(obj["odd"])
        even_week = Week.from_json(obj["even"])
        return f'{odd_week.pretty_print()}\n\n{even_week.pretty_print()}'

    def add_homework(self, subject: str, task: str):
        if subject.lower() == 'сд8-л':
            subject = 'СД №8 (лек.)'
        elif subject.lower() == 'сов-л':
            subject = 'Системы обнаружения вторжений (лек.)'
        elif subject.lower() == 'ситии-л':
            subject = 'Системы и технологии ИИ (лек.)'
        elif subject.lower() == 'нипози-л':
            subject = 'Нормативные и правовые основы ЗИ (лек.)'
        elif subject.lower() == 'мтвзс':
            subject = 'Моделирования трафика в ЗС'
        elif subject.lower() == 'сд3-л':
            subject = 'СД №3 (лек.)'
        elif subject.lower() == 'сд3-с':
            subject = 'СД №3 (сем.)'
        elif subject.lower() == 'мсиб-л':
            subject = 'Менеджмент систем ИБ (лек.)'
        elif subject.lower() == 'смиб':
            subject = 'Статистические модели ИБ'
        elif subject.lower() == 'ипиад-лаб':
            subject = 'Информационный поиск и анализ данных (лаб.)'
        elif subject.lower() == 'ипиад-л':
            subject = 'Информационный поиск и анализ данных (лек.)'
        elif subject.lower() == 'апо':
            subject = 'Анализ программных реализаций (лаб.)'
        elif subject.lower() == 'мсиб-с':
            subject = 'Менеджмент систем ИБ (сем.)'

        date = datetime.now() + timedelta(days=7)
        week_type = 'odd' if (HomeworkManager.__get_week_number(date) % 2 == 1) else 'even'

        with self.lock:
            with open(f'{self.work_file}', encoding='utf-8', mode='r') as f:
                obj = json.load(f)

            hw_week = Week.from_json(obj[f"{week_type}"])
            if hw_week.add_task(subject, task):
                obj[f"{week_type}"] = hw_week.to_json()
                with open(f'{self.work_file}', encoding='utf-8', mode='w') as f:
                    json.dump(obj, f, indent=4, ensure_ascii=False)
                return 'Добавил!'

            week_type = 'even' if (week_type == 'odd') else 'odd'

            hw_week = Week.from_json(obj[f"{week_type}"])
            if hw_week.add_task(subject, task):
                obj[f"{week_type}"] = hw_week.to_json()
                with open(f'{self.work_file}', encoding='utf-8', mode='w') as f:
                    json.dump(obj, f, indent=4, ensure_ascii=False)
                return 'Добавил!'
        return 'Ошибка: название предмета или его сокращение не найдено!'

    def get_homework(self, day_of_week: str = ''):
        if not day_of_week:
            date = datetime.now() + timedelta(days=1)
            if date.weekday() == 6:
                date += timedelta(days=1)
        else:
            if day_of_week.lower() == 'пн':
                day_num = 0
            elif day_of_week.lower() == 'вт':
                day_num = 1
            elif day_of_week.lower() == 'ср':
                day_num = 2
            elif day_of_week.lower() == 'чт':
                day_num = 3
            elif day_of_week.lower() == 'пт':
                day_num = 4
            elif day_of_week.lower() == 'сб':
                day_num = 5
            else:
                return 'Ошибка обработки дня недели'

            if datetime.now().weekday() > day_num:
                date = datetime.now() + timedelta(days=7 - (datetime.now().weekday() - day_num))
            else:
                date = datetime.now() + timedelta(days=day_num - datetime.now().weekday())

        day = date.weekday()
        week_type = 'odd' if (HomeworkManager.__get_week_number(date) % 2 == 1) else 'even'

        with self.lock:
            with open(f'{self.work_file}', encoding='utf-8', mode='r') as f:
                hw_week = Week.from_json(json.load(f)[f"{week_type}"])
        return hw_week.days[day].pretty_print()
