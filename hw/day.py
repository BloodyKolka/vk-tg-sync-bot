from dataclasses import dataclass

from hw.homework import HomeWork


@dataclass
class Day:
    day_of_week: str
    week_type: str
    subjects: list[HomeWork]

    def add_task(self, subject: str, task: str) -> bool:
        for subject_obj in self.subjects:
            if subject_obj.subject == subject:
                subject_obj.task = task
                return True
        return False

    def pretty_print(self):
        msg = f'=== {self.day_of_week} ({self.week_type}) ===\n'
        for subject in self.subjects:
            msg += subject.pretty_print()
            msg += '\n'
        return msg

    def to_json(self):
        obj = dict()
        obj["day_of_week"] = self.day_of_week
        obj["week_type"] = self.week_type
        obj["subjects"] = HomeWork.to_json_list(self.subjects)
        return obj

    @classmethod
    def to_json_list(cls, lst: list):
        obj = list()
        for item in lst:
            obj.append(item.to_json())
        return obj

    @classmethod
    def from_json(cls, obj):
        return cls(
            day_of_week=obj["day_of_week"],
            week_type=obj["week_type"],
            subjects=HomeWork.from_json_list(obj["subjects"])
        )

    @classmethod
    def from_json_list(cls, lst: list):
        res = list()
        for item in lst:
            res.append(cls.from_json(item))
        return res
