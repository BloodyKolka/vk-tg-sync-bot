from bot_threads import start_bot
from bot_threads import init_bot


from vk.vk_handler import run_vk_bot

from tg.tg_handlers import run_tg_bot
from tg.tg_sync import tg_sync


init_bot(vk=run_vk_bot, tg=run_tg_bot, tg_sync=tg_sync)
start_bot()
