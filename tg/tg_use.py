from telebot.types import InputFile
from telebot.types import InputMediaDocument
from telebot.types import InputMediaPhoto

import utils.file_utils as utils
from utils.poll_data import PollData

from tg.tg_vars import tg_bot_lock
from tg.tg_vars import tg_bot


def tg_get_user_tag(user_id, chat_id_name):
    try:
        chat_id = utils.get_from_file(chat_id_name)
        with tg_bot_lock:
            user = tg_bot.get_chat_member(chat_id=int(chat_id), user_id=int(user_id))
        return f'@{user.user.username}'
    except:
        return str()


def tg_send_message(message, chat: str, reply_id=0):
    chat_id = utils.get_from_file(chat)
    with tg_bot_lock:
        if reply_id == 0:
            sent_msg = tg_bot.send_message(chat_id, message, parse_mode='HTML')
        else:
            sent_msg = tg_bot.send_message(chat_id, message, parse_mode='HTML', reply_to_message_id=reply_id)

    return sent_msg.message_id


def tg_send_content(message, content, content_types, chat: str, reply_id=0):
    sent_msg = None
    chat_id = utils.get_from_file(chat)
    if len(content) == 1:
        if content_types[0] == 'doc':
            with tg_bot_lock:
                if reply_id == 0:
                    sent_msg = tg_bot.send_document(chat_id, InputFile(content[0]), caption=message,
                                                    parse_mode='HTML')
                else:
                    sent_msg = tg_bot.send_document(chat_id, InputFile(content[0]), caption=message,
                                                    parse_mode='HTML', reply_to_message_id=reply_id)
        elif content_types[0] == 'photo':
            with tg_bot_lock:
                if reply_id == 0:
                    sent_msg = tg_bot.send_photo(chat_id, InputFile(content[0]), caption=message, parse_mode='HTML')
                else:
                    sent_msg = tg_bot.send_photo(chat_id, InputFile(content[0]), caption=message, parse_mode='HTML',
                                                 reply_to_message_id=reply_id)
    elif len(content) >= 2:
        media = []
        for i in range(len(content)):
            if 'doc' not in content_types:
                media.append(InputMediaPhoto(InputFile(content[i])))
            else:
                media.append(InputMediaDocument(InputFile(content[i])))
        with tg_bot_lock:
            if message:
                if reply_id == 0:
                    sent_msg = tg_bot.send_message(chat_id, message, parse_mode='HTML')
                else:
                    sent_msg = tg_bot.send_message(chat_id, message, parse_mode='HTML',
                                                   reply_to_message_id=reply_id)
            if (not message) and (reply_id != 0):
                tmp = tg_bot.send_media_group(chat_id, media, reply_to_message_id=reply_id)
            else:
                tmp = tg_bot.send_media_group(chat_id, media)
            if not sent_msg:
                sent_msg = tmp
    return sent_msg.message_id


def tg_send_poll(poll: PollData, chat: str, reply_id=0):
    chat_id = utils.get_from_file(chat)
    with tg_bot_lock:
        if reply_id == 0:
            sent_msg = tg_bot.send_poll(chat_id, question=poll.title, options=poll.options, is_anonymous=poll.is_private, allows_multiple_answers=poll.is_multiple)
        else:
            sent_msg = tg_bot.send_poll(chat_id, question=poll.title, options=poll.options, is_anonymous=poll.is_private, allows_multiple_answers=poll.is_multiple, reply_to_message_id=reply_id)

    return sent_msg.message_id


def tg_send(chat: str, message = None, content = None, content_types = None, poll: PollData = None, reply_id = 0):
    ret = None
    if message and content:
        ret = tg_send_content(message, content, content_types, chat, reply_id)
    elif message:
        ret = tg_send_message(message, chat, reply_id)
    if poll:
        ret = tg_send_poll(poll, chat, reply_id)
    return ret
