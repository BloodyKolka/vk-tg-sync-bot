import time
import os
import json
import shutil

import utils.file_utils as utils

import tg.tg_vars as tg_vars

from vk.vk_use import vk_send_content


def tg_sync():
    while True:
        try:
            time.sleep(60)
            for current_media_group_id in os.listdir('./tg_tmp/group'):
                group_folder = './tg_tmp/group/' + current_media_group_id

                if not os.path.isdir(group_folder):
                    continue

                with tg_vars.tg_media_lock:
                    if current_media_group_id == tg_vars.media_group_id:
                        if time.time() - tg_vars.media_group_timestamp > 60:
                            tg_vars.media_group_id = ''
                        else:
                            continue

                f = open(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json')
                catalogue = json.load(f)

                msg = utils.get_from_file(group_folder + '/username_jd381hd091i2kd0323j13123fds')
                caption = utils.get_from_file(group_folder + '/msg_jd381hd091i2kd0323j13123fds')
                if caption:
                    msg += caption

                chat_id_filename = utils.get_from_file(group_folder + '/chat_id_filename_jd381hd091i2kd0323j13123fds')
                reply_id = int(utils.get_from_file(group_folder + '/reply_id_jd381hd091i2kd0323j13123fds'))
                vk_send_content(msg, catalogue["content"], catalogue["content_types"], chat_id_filename, reply_id)
                shutil.rmtree(f'./tg_tmp/group/{current_media_group_id}')
        except Exception as e:
            utils.save_exception('tg_sync', e)
            continue
        finally:
            continue
