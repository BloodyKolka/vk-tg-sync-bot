import json
import random

import telebot.types

from utils.poll_data import PollData
import utils.file_utils as utils

from tg.tg_vars import tg_bot
from tg.tg_vars import tg_bot_lock
from tg.tg_vars import tg_admins

from tg.tg_process import process_tg_message

from bot_threads import health_check

from global_vars import homework_manager


def remove_command(text: str, command: str):
    bot_tag = f'@{tg_bot.user.username}'

    command_less = text.replace(f'/{command} ', '')
    command_less = command_less.replace(f'/{command}', '')
    command_less = command_less.replace(f'/{command}{bot_tag} ', '')
    command_less = command_less.replace(f'/{command}{bot_tag}', '')
    return command_less


@tg_bot.message_handler(commands=['get_hw'])
def handle_get_hw(message: telebot.types.Message):
    chat_id = utils.get_from_file('./chat_ids/tg_chat_id')
    try:
        with tg_bot_lock:
            test = tg_bot.get_chat_member(chat_id, message.from_user.id)
    except:
        return

    if test.status == 'left':
        return

    args = remove_command(message.text, 'get_hw')
    if not args.replace(' ', ''):
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, homework_manager.get_homework())
    elif 'all' in args:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, homework_manager.get_all_homework())
    else:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, homework_manager.get_homework(args.replace(' ', '')))


@tg_bot.message_handler(commands=['register_hw'])
def handle_register(message: telebot.types.Message):
    if message.chat.id != message.from_user.id:
        return
    key = remove_command(message.text, 'register_hw')
    if key == 'x5WQzhnXY9csm5PRn37PfxUdeskcKS5iK57HnCFt':
        utils.add_hw_provider(message.from_user.id, 'tg')
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Отлично, теперь у тебя есть права добалять ДЗ!')
    else:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Ошибка! Неверный ключ!')


@tg_bot.message_handler(commands=['add_hw'])
def handle_get_hw(message: telebot.types.Message):
    if message.from_user.id not in utils.get_hw_providers('tg'):
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Ошибка, у тебя нет прав добавлять ДЗ')
        return

    args_str = remove_command(message.text, 'add_hw')
    subject = args_str.split(' ')[0]
    task = args_str.replace(subject + ' ', '')

    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, homework_manager.add_homework(subject, task))


@tg_bot.message_handler(commands=['del'])
def handle_delete(message):
    if message.reply_to_message is not None:
        if message.reply_to_message.from_user.id == tg_bot.user.id:
            tg_bot.delete_message(message.reply_to_message.chat.id, message.reply_to_message.message_id)


@tg_bot.message_handler(commands=['add_auf'])
def handle_add_auf(message):
    if message.from_user.id not in tg_admins:
        return

    new_quote = message.text.replace('/add_auf ', '')

    f = open('./data/wolf_quotes.json')
    quotes_file = json.load(f)
    f.close()

    quotes_file["quotes"].append(new_quote)

    utils.save_to_file('./data/wolf_quotes.json', json.dumps(quotes_file, indent=4))


@tg_bot.message_handler(commands=['auf'])
def handle_auf(message):
    f = open('./data/wolf_quotes.json')
    quotes = json.load(f)["quotes"]
    f.close()

    msg = f'"{random.choice(quotes)}" - АУФ.'
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, msg)


@tg_bot.message_handler(commands=['health_check'])
def handle_health_check(message):
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, health_check())


@tg_bot.message_handler(commands=['start_important'])
def handle_start_important(message):
    if message.from_user.id not in tg_admins:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Упс! У Вас нет на это прав!')
        return
    utils.save_to_file('./chat_ids/tg_imp_id', message.chat.id)
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, 'Добавил к синхронизации Важное')


@tg_bot.message_handler(commands=['start_chat'])
def handle_start_chat(message): 
    if message.from_user.id not in tg_admins:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Упс! У Вас нет на это прав!')
        return
    utils.save_to_file('./chat_ids/tg_chat_id', message.chat.id)
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, 'Добавил к синхронизации Разговорная')


@tg_bot.message_handler(commands=['start_rpg'])
def handle_start_rpg(message):
    if message.from_user.id not in tg_admins:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, 'Упс! У Вас нет на это прав!')
        return
    utils.save_to_file('./chat_ids/tg_rpg_id', message.chat.id)
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, 'Добавил к синхронизации НРИ')


@tg_bot.message_handler(commands=['callme'])
def handle_callme(message):
    nick = message.text.replace('/callme ', '')
    if not nick:
        return

    f = open('./data/tg_nicknames.json')
    nicknames = json.load(f)
    nicknames[str(message.from_user.id)] = nick
    f.close()
    utils.save_to_file('./data/tg_nicknames.json', json.dumps(nicknames, indent=4))

    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, f'Теперь в ВК я буду подписывать Ваши сообщения "{nick}"',
                            reply_to_message_id=message.message_id)


@tg_bot.message_handler(commands=['set_tag'])
def handle_callme(message):
    new_tag = message.text.replace('/set_tag ', '')
    new_tag = new_tag.replace(' ', '')
    new_tag = new_tag.lower()

    if not new_tag:
        return
    new_tag = '$' + new_tag

    f = open('./data/tg_tags.json')
    tags = json.load(f)
    f.close()

    if new_tag in tags:
        with tg_bot_lock:
            tg_bot.send_message(message.chat.id, f'Упс! Тег "{new_tag}" уже занят!',
                                reply_to_message_id=message.message_id)
            return

    tag_to_delete = ''
    for tag in tags:
        if tags[tag] == str(message.from_user.id):
            tag_to_delete = tag
            break
    if tag_to_delete:
        del tags[tag_to_delete]

    tags[new_tag] = str(message.from_user.id)
    utils.save_to_file('./data/tg_tags.json', json.dumps(tags, indent=4))
    with tg_bot_lock:
        tg_bot.send_message(message.chat.id, f'Теперь в ВК Вас можно тегнуть с помощью "{new_tag}"',
                            reply_to_message_id=message.message_id)


@tg_bot.message_handler(content_types=['text', 'photo', 'document', 'sticker', 'poll'])
def handle_message(message: telebot.types.Message):
    if message.text:
        txt = message.text
    elif message.caption:
        txt = message.caption
    elif 'poll' in message.content_type:
        if message.poll.explanation:
            txt = message.poll.explanation
        else:
            txt = ""
    else:
        txt = ""
    if '#nosync' in txt:
        return

    chat_id_filename = ''
    val = utils.get_from_file('./chat_ids/tg_chat_id')
    if val == str(message.chat.id):
        chat_id_filename = './chat_ids/vk_chat_id'
    val = utils.get_from_file('./chat_ids/tg_imp_id')
    if val == str(message.chat.id):
        chat_id_filename = './chat_ids/vk_imp_id'
    val = utils.get_from_file('./chat_ids/tg_rpg_id')
    if val == str(message.chat.id):
        chat_id_filename = './chat_ids/vk_rpg_id'
    if not chat_id_filename:
        return

    reply_id = 0
    if message.reply_to_message:
        reply_id = process_tg_message(chat_id_filename, message.reply_to_message, 0, True)
    process_tg_message(chat_id_filename, message, reply_id, False)


def run_tg_bot():
    while True:
        try:
            tg_bot.infinity_polling()
        except Exception as e:
            utils.save_exception('run_tg_bot', e)
            continue
        finally:
            continue
