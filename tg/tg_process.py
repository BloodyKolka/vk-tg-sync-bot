import json
import shutil
import time
import os

import pathlib
import lottie
import lottie.exporters.gif as lottie_gif_exporters
import telebot
from moviepy.editor import VideoFileClip

import utils.file_utils as utils
from utils.poll_data import PollData

import tg.tg_vars as tg_vars
from tg.tg_vars import tg_media_lock
from tg.tg_vars import tg_bot_lock
from tg.tg_vars import tg_bot
from tg.tg_vars import tg_bot_token

from vk.vk_use import vk_send
from vk.vk_use import vk_get_user_tag


def process_tg_message(chat_id_filename, message: telebot.types.Message, reply_id, is_reply):
    username = ''
    if not message.from_user.is_bot:
        f = open('./data/tg_nicknames.json')
        nicknames = json.load(f)
        f.close()
        if str(message.from_user.id) in nicknames:
            username = f'{nicknames[str(message.from_user.id)]}:\n'
        else:
            username = f'{message.from_user.first_name} {message.from_user.last_name}:\n'
    if message.forward_sender_name:
        username += f'[forwarding]\n{message.forward_sender_name}:\n'
    elif message.forward_from_chat:
        username += f'[forwarding]\n{message.forward_from_chat.title}:\n'

    msg = username

    if message.text:
        tmp = message.text

        f = open('./data/vk_tags.json')
        tags = json.load(f)
        f.close()
        for tag in tags:
            if tag in tmp:
                user_tag = vk_get_user_tag(tags[tag])
                if not user_tag:
                    continue
                tmp = tmp.replace(tag, user_tag)

        msg += tmp

    poll_data = None
    if 'poll' in message.content_type:
        tg_poll: telebot.types.Poll = message.poll
        answers = list()
        for answer in tg_poll.options:
            answers.append(answer.text)
        poll_data = PollData(tg_poll.question, tg_poll.explanation, answers, tg_poll.is_anonymous, tg_poll.allows_multiple_answers)

    if (not (message.photo or message.document or message.sticker)) and (not tg_vars.media_group_id):
        return vk_send(chat_id_filename, message=msg, poll=poll_data, reply_to=reply_id)

    current_media_group_id = ''
    if message.media_group_id:
        current_media_group_id = message.media_group_id

    with tg_media_lock:
        if (current_media_group_id != tg_vars.media_group_id) and tg_vars.media_group_id:
            tg_vars.media_group_timestamp = 0

            group_folder = './tg_tmp/group/' + tg_vars.media_group_id
            f = open(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json')
            catalogue = json.load(f)

            msg = utils.get_from_file(group_folder + '/username_jd381hd091i2kd0323j13123fds')
            caption = utils.get_from_file(group_folder + '/msg_jd381hd091i2kd0323j13123fds')
            if caption:
                msg += caption

            vk_send(chat_id_filename, message=msg, content=catalogue["content"], content_types=catalogue["content_types"], poll=poll_data, reply_to=reply_id)
            shutil.rmtree(f'./tg_tmp/group/{tg_vars.media_group_id}')

            tg_vars.media_group_id = ''

            return process_tg_message(chat_id_filename, message, reply_id, is_reply)

    if message.caption:
        msg = msg + message.caption

    file_id = ''
    file_type = ''
    file_name = ''
    if message.document:
        file_id = message.document.file_id
        if message.document.file_name:
            file_name = message.document.file_name
        file_type = 'doc'
    elif message.photo:
        max_file_size = 0
        for photo in message.photo:
            if photo.file_size > max_file_size:
                file_id = photo.file_id
        file_type = 'photo'
    elif message.sticker:
        file_id = message.sticker.file_id
        file_type = 'photo'

    file = tg_bot.get_file(file_id)
    file_url = f'https://api.telegram.org/file/bot{tg_bot_token}/{file.file_path}'
    if not file_name:
        file_name = os.path.basename(file.file_path)

    name = './tg_tmp'

    with tg_media_lock:
        if is_reply or (not message.media_group_id and not tg_vars.media_group_id):
            name = name + '/' + file_name
            utils.download_and_save_file(file_url, name)

            if pathlib.Path(name).suffix == '.tgs':
                tgs = lottie.parsers.tgs.parse_tgs(name)
                lottie_gif_exporters.export_gif(tgs, name + '.gif', skip_frames=3)
                os.remove(name)
                name = name + '.gif'
                file_type = 'doc'
            elif pathlib.Path(name).suffix == '.webm':
                video_clip = VideoFileClip(name)
                video_clip.write_gif(name + '.gif')
                os.remove(name)
                name = name + '.gif'
                file_type = 'doc'

            repl = vk_send(chat_id_filename, message=msg, content=[name], content_types=[file_type], poll=poll_data, reply_to=reply_id)
            os.remove(name)
            return repl

    if (not message.media_group_id) and (not tg_vars.media_group_id):
        return ''

    name += '/group'
    with tg_media_lock:
        tg_vars.media_group_timestamp = time.time()
        tg_vars.media_group_id = current_media_group_id

    group_folder = name + '/' + current_media_group_id
    os.makedirs(group_folder, exist_ok=True)
    if message.caption:
        utils.save_to_file(group_folder + '/msg_jd381hd091i2kd0323j13123fds', message.caption)
    utils.save_to_file(group_folder + '/username_jd381hd091i2kd0323j13123fds', username)
    utils.save_to_file(group_folder + '/chat_id_filename_jd381hd091i2kd0323j13123fds', chat_id_filename)
    utils.save_to_file(group_folder + '/reply_id_jd381hd091i2kd0323j13123fds', reply_id)

    name = group_folder + '/' + file_name

    with tg_bot_lock:
        if not os.path.exists(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json'):
            utils.save_to_file(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json',
                               f'{{"content": [], "content_types": []}}')

    f = open(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json')
    catalogue = json.load(f)
    f.close()
    catalogue["content"].append(name)
    catalogue["content_types"].append(file_type)
    utils.save_to_file(group_folder + '/content_catalogue_jd381hd091i2kd0323j13123fds.json', json.dumps(catalogue))
    utils.download_and_save_file(file_url, name)
